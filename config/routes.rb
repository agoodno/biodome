Rails.application.routes.draw do
  root to: 'visitors#index'
  get '/visitors/edit_dashboard', to: 'visitors#edit_dashboard'
  put '/visitors/update_dashboard', to: 'visitors#update_dashboard'
  devise_for :users
  resources :users
end
