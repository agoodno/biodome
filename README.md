# Biodome

Programming Task. In the spirit of semi-random project names I just
chose Biodome.

## Setup

### Clone the project
    cd ~/src/
    git clone git@gitlab.com:agoodno/biodome.git
    cd biodome

### Setup the project to run
    bundle install
    rake db:migrate
    rake db:seed

## Run
    bundle exec rails server

## Login users
The login had to be an email to be compatible with the Devise gem so
the users and passwords are:

    user1@wisc.edu/glbrc1
    user2@wisc.edu/glbrc2
    user3@wisc.edu/glbrc3

Each is setup with a different set of applications on their dashboard to start.

## Apps

1. Can be clicked to launch a new tab to the site
2. Can be edited using the Edit Apps link and selecting the ones that
   should show on the dashboard.
3. Can be sorted by using the Edit Apps link and dragging and dropping
   to the new location
