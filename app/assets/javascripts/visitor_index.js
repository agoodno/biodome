$().ready(function() {
    $('.info').click(function() {
        var win = window.open($(this).attr("data-href"), '_blank');
        if (win) {
            win.focus();
        } else {
            alert('Please allow popups for this website');
        }
        return false;
    });

    $("#apps-list").sortable();
});
