class VisitorsController < ApplicationController
  before_action :authenticate_user!

  def edit_dashboard
    # Retrieve selected apps for this user in order
    @apps = App.joins(:app_users).where(app_users: { user: current_user }).order('app_users.position')
    # Retrieve unselected apps
    @apps = @apps + App.where.not(id: @apps.map(&:id))
  end

  def update_dashboard
    current_user.app_users.destroy_all
    params[:app_ids].each_with_index do |app_id, idx|
      current_user.app_users.create!(app: App.find(app_id), position: idx)
    end
    redirect_to root_path
  end

end
