class CreateAppUserService
  def call_with(params)
    user = User.find_by!(email: params[:email])
    user.app_users |= params[:app_users]
  end
end
