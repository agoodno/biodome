class CreateAdminService
  def call
    User.find_or_create_by!(email: Rails.application.secrets.admin_email) do |user|
      user.password = Rails.application.secrets.admin_password
      user.password_confirmation = Rails.application.secrets.admin_password
    end
  end

  def call_with(params)
    User.find_or_create_by!(email: params[:email]) do |user|
      user.password = params[:password]
      user.password_confirmation = params[:password]
    end
  end
end
