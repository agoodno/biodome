class CreateAppService
  def call_with(params)
    App.find_or_create_by!(name: params[:name]) do |app|
      app.description = params[:description]
      app.color = params[:color]
      app.defaultStatus = params[:defaultStatus]
      app.link = params[:link]
    end
  end
end
