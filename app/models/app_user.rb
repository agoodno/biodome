class AppUser < ApplicationRecord
  validates :position, presence: true

  belongs_to :app
  belongs_to :user
end
