class App < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  validates :color, presence: true
  validates :defaultStatus, inclusion: { in: [true, false] }
  validates :link, presence: true
  has_many :app_users
  has_many :users, through: :app_users
end
