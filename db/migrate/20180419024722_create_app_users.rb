class CreateAppUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :app_users do |t|
      t.references :app, null: false, index: true
      t.references :user, null: false, index: true
      t.integer :position, null: false, default: Random.rand(1)

      t.timestamps
    end

    drop_table :apps_users
  end
end
