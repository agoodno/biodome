class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t|
      t.string :name, null: false, unique: true
      t.text :description, null: false
      t.string :color, null: false
      t.boolean :defaultStatus, null: false
      t.string :link, null: false

      t.timestamps
    end

    add_index :apps, :name, :unique => true
  end
end
