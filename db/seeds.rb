# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'CREATED ADMIN USER: ' << CreateAdminService.new.call.email
puts 'CREATED ADMIN USER: ' << CreateAdminService.new.call_with(email: 'user1@wisc.edu', password: "glbrc1").email
puts 'CREATED ADMIN USER: ' << CreateAdminService.new.call_with(email: 'user2@wisc.edu', password: "glbrc2").email
puts 'CREATED ADMIN USER: ' << CreateAdminService.new.call_with(email: 'user3@wisc.edu', password: "glbrc3").email

CreateAppService.new.call_with(name: 'Google', description: 'Search Engine', color: 'indianred', defaultStatus: true, link: 'http://www.google.com')
CreateAppService.new.call_with(name: 'Wisc', description: 'UW homepage', color: 'lightblue', defaultStatus: false, link: 'http://www.wisc.edu')
CreateAppService.new.call_with(name: 'GLBRC', description: 'Great Lakes Bioenergy Research Center', color: 'lightyellow', defaultStatus: true, link: 'http://www.glbrc.org')
CreateAppService.new.call_with(name: 'WEI', description: 'Wisconsin Energy Institute', color: 'lightgreen', defaultStatus: false, link: 'https://energy.wisc.edu/')
CreateAppService.new.call_with(name: 'Twitter', description: 'Twitter', color: 'mediumpurple', defaultStatus: false, link: 'https://twitter.com/')

CreateAppUserService.new.call_with(email: 'user1@wisc.edu', app_users: [
                                      AppUser.new(user: User.find_by(email: 'user1@wisc.edu'), app: App.find_by(name: 'Google'), position: 3),
                                      AppUser.new(user: User.find_by(email: 'user1@wisc.edu'), app: App.find_by(name: 'Wisc'), position: 1),
                                      AppUser.new(user: User.find_by(email: 'user1@wisc.edu'), app: App.find_by(name: 'Twitter'), position: 2)
                                   ])

CreateAppUserService.new.call_with(email: 'user2@wisc.edu', app_users: [
                                      AppUser.new(user: User.find_by(email: 'user2@wisc.edu'), app: App.find_by(name: 'Google'), position: 2),
                                      AppUser.new(user: User.find_by(email: 'user2@wisc.edu'), app: App.find_by(name: 'GLBRC'), position: 1)
                                   ])

CreateAppUserService.new.call_with(email: 'user3@wisc.edu', app_users: [
                                      AppUser.new(user: User.find_by(email: 'user3@wisc.edu'), app: App.find_by(name: 'Google'), position: 4),
                                      AppUser.new(user: User.find_by(email: 'user3@wisc.edu'), app: App.find_by(name: 'Wisc'), position: 2),
                                      AppUser.new(user: User.find_by(email: 'user3@wisc.edu'), app: App.find_by(name: 'GLBRC'), position: 5),
                                      AppUser.new(user: User.find_by(email: 'user3@wisc.edu'), app: App.find_by(name: 'WEI'), position: 1),
                                      AppUser.new(user: User.find_by(email: 'user3@wisc.edu'), app: App.find_by(name: 'Twitter'), position: 3)
                                   ])
